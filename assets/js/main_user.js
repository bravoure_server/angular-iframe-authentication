$(function () {

    if ($('#logged_in').val() == '1') {
        window.parent.postMessage({
            'is_logged_in': true,
            'access_token': $('#access_token').val(),
            'refresh_token': $('#refresh_token').val(),
            'first_name': $('#first_name').val(),
            'last_name': $('#last_name').val()
        }, "*");
    }

    var errorClass = 'has-error';
    var successClass = 'has-no-error';

    var toValidate;

    var formUserAction = formUserAction || {},
        $input_container = $('.form-group'),
        $this;

    formUserAction = {
        initialize: function () {

            $this = this;

            // updated the iframe size
            $this.sendHeightIframeDefault();


            // Meer Muziek
            $this.checkLabelPosition();
            $this.checkMusicTeacherPosition();

            // Adds funcitonalities to prevent links and open them in new tab
            $this.checkRedirectLinks();

            /* Moves the labels to the top */
            $input_container.on('click focus', function () {
                $this.setLabelActive($(this));
            });

            /* when using tabs */
            $input_container.find('input, select').on('focus', function (e) {
                $this.setContainerActive($(this));
            });

            /* when leaving the field if the input is empty puts back the label into its original position*/
            $input_container.find('input, select').on('blur', function () {
                $this.setLabelInactive($(this));
            });

            $('.userForm').submit($this.formSubmit);

            $('[data-validate]').on('blur keyup change mousedown', $this.validateFields);
        },

        checkMusicTeacherPosition: function () {
            $this.checkMusicTeacherVisibility();

            /*$('label[for=music_teacher_checkbox]').on('click', function () {
                setTimeout(function(){
                    $this.checkMusicTeacherVisibility();
                },300);
            });*/

            $('select[name=music_teacher_checkbox]').on('change', function () {

                setTimeout(function(){
                    $this.checkMusicTeacherVisibility();
                },300);

                console.log ($(this).val());
                /*


                if ($(this).val() == 1) {
                    $('select[name="contact_music_teacher_checkbox"]').parent().fadeOut('slow');
                    $('input[name="music_teacher_name"]').parent().fadeOut('slow');
                } else {
                    $('select[name="contact_music_teacher_checkbox"]').parent().fadeIn('slow');
                    $('input[name="music_teacher_name"]').parent().fadeIn('slow');
                }*/

            });
        },

        checkRedirectLinks: function () {

            // links with redirect-link class will be prevented and open the link in a new tab
            $(document).on('click', '.redirect-link', function (e) {
                e.preventDefault();

                var url = $(this).attr('href');
                window.parent.postMessage({
                    'redirectUrl': url
                }, "*");
            });
        },

        checkMusicTeacherVisibility: function () {
            var $musicTeacherValue = $('select[name=music_teacher_checkbox]').val();
            var $musicTeacherName = $('#music_teacher_name').parent();
            var $musicTeacherEmail = $('input[name="music_teacher_email"]').parent();
            var $contactmusicTeacherEmail = $('select[name="contact_music_teacher_checkbox"]').parent();
            var $formText = $('.form-text');

            if ($musicTeacherValue == 0) {
                $musicTeacherName.removeClass('hidden');
                $contactmusicTeacherEmail.removeClass('hidden');
                $musicTeacherEmail.removeClass('hidden');
                $formText.removeClass('hidden');
            } else {
                $musicTeacherName.addClass('hidden');
                $contactmusicTeacherEmail.addClass('hidden');
                $musicTeacherEmail.addClass('hidden');
                $formText.addClass('hidden');
            }

            $this.sendHeightIframe();
        },

        validateEmail: function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },

        validationError: function (obj) {
            obj.addClass(errorClass).removeClass(successClass);
        },

        validationSuccess: function (obj) {
            obj.removeClass(errorClass).addClass(successClass);
        },

        validationType: function (type, obj) {

            switch (type) {
                /* REQUIRED CHECKBOX CASE */
                case 'required_checkbox':

                    var result;
                    var value = obj.find('.form-control').is(':checked');

                    if (!value) {
                        $this.validationError(obj);
                        result = false;
                    } else {
                        $this.validationSuccess(obj);
                        result = true;
                    }

                    return result;
                    break;
                /* REQUIRED CASE */
                case 'required':

                    var result;
                    var value = obj.find('.form-control').val();

                    if (value == '') {
                        $this.validationError(obj);
                        result = false;
                    } else {
                        $this.validationSuccess(obj);
                        result = true;
                    }

                    return result;
                    break;

                /* PASSWORD CASE */
                case 'password':

                    var result;
                    var value = obj.find('.form-control').val();

                    if (value == '' || value.length < 8) {
                        $this.validationError(obj);
                        result = false;
                    } else {
                        $this.validationSuccess(obj);
                        result = true;
                    }

                    return result;
                    break;
                case 'email':
                    /* EMAIL CASE */

                    var value = obj.find('.form-control').val();

                    if (value == '') {
                        $this.validationError(obj);
                        result = false;
                    } else {
                        if ($this.validateEmail(value)) {
                            $this.validationSuccess(obj);
                            result = true;
                        } else {
                            $this.validationError(obj);
                            result = false;
                        }
                    }

                    return result;
                    break;
            }
        },

        validateFields: function () {
            if (toValidate) {
                var $this_obj = $(this).parent();
                var type = $this_obj.find('[data-validate]').data('validate');

                $this.validationType(type, $this_obj);

                $this.sendHeightIframe();
            }

            $this.checkMusicTeacherPosition();
        },

        formSubmit: function (e) {
            e.preventDefault();
            toValidate = true;

            $this.sendHeightIframe();

            // Gets the items with "data-validate" defined and gets the parent.
            var items = $('[data-validate]').parent();
            var result = true;

            $.each(items, function (index) {
                var type = $(this).find('[data-validate]').data('validate');

                $this.validationType(type, $(this));

                if (!$this.validationType(type, $(this))) {
                    result = false;
                }

            });

            if (result) {
                $(this).unbind('submit').submit();
            }
        },

        setLabelActive: function (object) {
            //object.find('label').addClass('active').next().focus();

            object.parents('.form-group').addClass('focused');
        },

        setContainerActive: function (object) {

            /*object.parent().find('label').addClass('active');
            $input_container.removeClass('active');
            object.parents('.input_container').addClass('active');*/


            object.parents('.form-group').addClass('focused');
        },

        setLabelInactive: function (object) {
            if (object.val() == '') {
                object.parents('.form-group').removeClass('focused');
            }
        },

        checkLabelPosition: function () {
            $input_container.each(function () {
                var this_object = $(this);

                if (this_object.find('select').val() != undefined) {

                    if (this_object.find('select').val() != '') {
                        //this_object.find('label').addClass('active');
                        this_object.addClass('focused');
                    } else {
                        this_object.removeClass('focused');
                        //this_object.find('label').removeClass('active');
                    }
                }

                if (this_object.find('input').val() != undefined) {

                    if (this_object.find('input').val() != '') {
                        //this_object.find('label').addClass('active');
                        this_object.addClass('focused');
                    } else {
                        //this_object.find('label').removeClass('active');
                        this_object.removeClass('focused');
                    }
                }

                $this.sendHeightIframe();
            });
        },

        checkInputs: function () {
            var $inputs = $('input');

            $inputs.each(function () {

                if ($(this).attr('type') == 'password') {

                    if ($(this).val().length != 0) {
                        $this.setContainerActive($(this));
                    }
                }

                $this.sendHeightIframe();
            });
        },

        sendHeightIframeDefault: function () {

            window.parent.postMessage({
                'id': $('#form_container').data('id'),
                'height': $('#form_container').height()
            }, "*");
        },

        sendHeightIframe: function () {

            $this.sendHeightIframeDefault();

            setTimeout(function(){
                window.parent.postMessage({
                    'id': $('#form_container').data('id'),
                    'height': $('#form_container').height()
                }, "*");
            }, 300);
        }
    };

    $('form').existance(function () {
        formUserAction.initialize();
    });


    /*// deactivate confirmation
    var deactivateBtn = $('.js-deactivate'),
        deactivateMessage = deactivateBtn.data('deactivate');

    deactivateBtn.on('click', function() {
        if (window.confirm(deactivateMessage)) {

        } else {
            return false;
        }

    });*/

});


jQuery.fn.existance = function (func) {
    this.length && func.apply(this);
    return this;
};

